const axios = require('axios').default;
const qs = require('qs');
export default {
    loginUser (form, bvToast, cb) {
        axios.post('http://localhost:8000/login/', qs.stringify(form))
          .then(function (response) {
            cb(response.data.access_token)
          })
          .catch(function (error) {
            bvToast.toast(error.response.data.detail, {
              title: `Problem`,
              variant: 'danger',
              toaster: 'b-toaster-top-center',
              autoHideDelay: 3000,
              solid: true
            })
          });
    }
  }