const axios = require('axios').default;

export default {
    registerAdminUser (form, bvToast, cb, errorCb) {

        axios.post('http://localhost:8000/create-admin-user/', {
            email: form.email,
            password: form.password
          })
          .then(function () {
            bvToast.toast("New account created", {
                title: `Success`,
                variant: 'success',
                toaster: 'b-toaster-top-center',
                autoHideDelay: 3000,
                solid: true
              })                 
            cb()
          })
          .catch(function (error) {
            errorCb()
            bvToast.toast(error.response.data.detail, {
                title: `Problem`,
                variant: 'danger',
                toaster: 'b-toaster-top-center',
                autoHideDelay: 3000,
                solid: true
              })            
          });
    },
    registerUser (form, bvToast, cb, errorCb) {

        axios.post('http://localhost:8000/users/', {
            email: form.email,
            password: form.password
          })
          .then(function () {
            bvToast.toast("New account created", {
                title: `Success`,
                variant: 'success',
                toaster: 'b-toaster-top-center',
                autoHideDelay: 3000,
                solid: true
              })                 
            cb()
          })
          .catch(function (error) {
            errorCb()
            bvToast.toast(error.response.data.detail, {
                title: `Problem`,
                variant: 'danger',
                toaster: 'b-toaster-top-center',
                autoHideDelay: 3000,
                solid: true
              })            
          });
    }    
  }