const axios = require('axios').default;

export default {
  getLeaves(cb) {
    let config = {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem("access_token")
      }
    }

    axios.get('http://localhost:8000/leave/', config)
      .then(function (response) {
        cb(response.data)
      })
      .catch(function (error) {
        console.log(error)
      });
  },

  addLeave(body, cb) {
    let config = {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem("access_token")
      }
    }

    axios.post('http://localhost:8000/leave/', body, config)
      .then(function () {
        cb()
      })
      .catch(function (error) {
        console.log(error)
      });
  },
  updateLeave(uuid, body, cb) {
    let config = {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem("access_token")
      }
    }
    axios.patch('http://localhost:8000/leave/' + uuid, body, config)
      .then(function () {
        cb()
      })
      .catch(function (error) {
        console.log(error)
      });
  },

  deleteLeave(uuid, bvToast, cb) {
    let config = {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem("access_token")
      }
    }
    axios.delete('http://localhost:8000/leave/' + uuid, config)
      .then(function () {
        cb()
      })
      .catch(function (error) {
        bvToast.toast(error.response.data.detail, {
          title: `Problem`,
          variant: 'danger',
          toaster: 'b-toaster-top-center',
          autoHideDelay: 3000,
          solid: true
        })
      });
  }
}