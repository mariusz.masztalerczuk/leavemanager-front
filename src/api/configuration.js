const axios = require('axios').default;

export default {
    getStatus (next) {
        axios.get('http://localhost:8000/status/', {
          })
          .then(function (response) {
            if (response.data.status == "CONFIGURED") {
              next()
            } else { 
              next({ name: 'CreateAdminUserPage' })
            }
          })
          .catch(function (error) {
            console.log(error)
          });
    }
  }