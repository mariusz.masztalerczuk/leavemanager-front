const axios = require('axios').default;

export default {
  getUsers (cb) {
        let config = {
          headers: {
            'Authorization': 'Bearer ' + localStorage.getItem("access_token")
          }
        }
        
        axios.get('http://localhost:8000/users/', config)
          .then(function (response) {
            cb(response.data)
          })
          .catch(function (error) {
            console.log(error)
          });
    },

    updateUsers(uuid, body, cb) {
      let config = {
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem("access_token")
        }
      }

      axios.patch('http://localhost:8000/users/' + uuid, body, config)
        .then(function (response) {
          console.log(response.data)
          console.log(cb)
        })
        .catch(function (error) {
          console.log(error)
        });
  }     
   
}