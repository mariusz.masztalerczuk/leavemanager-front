import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'
import LoginPage from './components/LoginPage.vue'
import Dashboard from './components/Dashboard.vue'
import AdminDashboard from './components/AdminDashboard.vue'
import CreateAdminUserPage from './components/CreateAdminUserPage.vue'
import Register from './components/Register.vue'
import ManagerDashboard from './components/ManagerDashboard.vue'
import configuration from './api/configuration'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;
Vue.use(VueRouter)

const routes = [
  {
    path: '/', component: LoginPage, name: "Login", beforeEnter: (to, from, next) => {
      configuration.getStatus(next)
    }
  },
  { path: '/configure', name: "CreateAdminUserPage", component: CreateAdminUserPage },
  {
    path: '/dashboard', name: "Dashboard", component: Dashboard, beforeEnter: (to, from, next) => {
      let token = localStorage.getItem("access_token")
      if (token == null) next({ name: 'Login' })
      next()
    }
  },
  {
    path: '/admin-dashboard', name: "AdminDashboard", component: AdminDashboard, beforeEnter: (to, from, next) => {
      let token = localStorage.getItem("access_token")
      if (token == null) next({ name: 'Login' })
      next()
    }
  },
  { path: '/register', name: "Register", component: Register },
  {
    path: '/manager-dashboard', name: "ManagerDashboard", component: ManagerDashboard, beforeEnter: (to, from, next) => {
      let token = localStorage.getItem("access_token")
      if (token == null) next({ name: 'Login' })
      next()
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
