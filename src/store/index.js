import Vue from 'vue'
import Vuex from 'vuex'
import register from './modules/register'
import configuration from './modules/configuration'
import login from './modules/login'
import leaves from './modules/leaves'
import users from './modules/users'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    configuration,
    leaves,
    login,
    users,
    register
  },
  strict: debug
})