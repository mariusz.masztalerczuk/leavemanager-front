import leaves from '../../api/leaves'
import users from '../../api/users'
const state = () => ({
    leaves: []
  })

const actions = {
    getLeaves ({ commit }) {
      leaves.getLeaves(
        (leaves) => commit('setLeaves', leaves)
      )
    },

    deleteLeave ({ commit }, {uuid, bvToast}) {
      leaves.deleteLeave(
        uuid,
        bvToast,
        () => { leaves.getLeaves(
          (leaves) => commit('setLeaves', leaves)
        )}
      )
    },

    updateLeave ({ commit }, {uuid, body}) {
      leaves.updateLeave(
        uuid,
        body,
        () => { leaves.getLeaves(
          (leaves) => commit('setLeaves', leaves)
        )}
      )
    },

    updateLeaveAndUsers ({ commit }, {uuid, body}) {
      leaves.updateLeave(
        uuid,
        body,
        () => { users.getUsers(
          (users) => commit('users/setUsers', users, {root: true})
        )}
      )
    },

    addLeave ({ commit }, body) {
      leaves.addLeave(
        body,
        () => { leaves.getLeaves(
          (leaves) => commit('setLeaves', leaves)
        ) }
      )

    }
}

const mutations = {
  setLeaves (state, leaves) {
    console.log("muteate")
      state.leaves = leaves
    }
  }

const getters = {
    leaves: (state) => {
        return state.leaves
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}