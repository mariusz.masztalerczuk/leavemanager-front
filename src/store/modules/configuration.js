import status from '../../api/configuration'

const state = () => ({
    status: null
  })

const actions = {
    getStatus ({ commit }) {
      status.getStatus(
        (status) => commit('setStatus', status)
      )
    }
}

const mutations = {
    setStatus (state, status) {
      state.status = status
    }
  }

const getters = {
    status: (state) => {
        return state.status
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }