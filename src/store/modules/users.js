import users from '../../api/users'

const state = () => ({
    users: []
  })

const actions = {
    getUsers ({ commit }) {
      users.getUsers(
        (users) => commit('setUsers', users)
      )
    },

    updateUser ({ commit }, {uuid, body}) {
      users.updateUsers(
        uuid,
        body,
        (token) => { commit('setToken', token); }
      )
      users.getUsers(
        (users) => commit('setUsers', users)
      )
    },
}

const mutations = {
  setUsers (state, users) {
    console.log(users)
      state.users = users
    }
  }

const getters = {
  users: (state) => {
        return state.users
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}