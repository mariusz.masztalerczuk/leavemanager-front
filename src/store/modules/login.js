import login from '../../api/login'

const state = () => ({
    token: null
  })

const actions = {
    loginUser ({ commit }, {form, router, bvToast}) {
      login.loginUser(
        form,
        bvToast,
        (token) => { commit('setToken', token), router.push({ path: '/dashboard' })}
      )
    }
}

const mutations = {
    setToken (state, token) {
      localStorage.setItem('access_token', token)
      state.token = token
    },
    tryToLoadToken(state) {
      mutations.setToken(state, localStorage.getItem("access_token"))
    },
    logout (state) {
      localStorage.removeItem('access_token')
      state.token = null
    }
  }

function parseJwt(token) {
  var base64Payload = token.split('.')[1];
  var payload = Buffer.from(base64Payload, 'base64');
  return JSON.parse(payload.toString());
}

const getters = {
    token: (state) => {
        return state.token
    },
    is_user_admin: (state) => {
      if (state.token == null) {
        return false
      }
      let json = parseJwt(state.token)
      return json['admin']
    },
    is_user_manager: (state) => {
      if (state.token == null) {
        return false
      }
      let json = parseJwt(state.token)
      return json['manager']
    }    

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}