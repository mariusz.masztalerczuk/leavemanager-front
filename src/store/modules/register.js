import register from '../../api/register'

const state = () => ({
    items: [],
    registerStatus: null
})

const actions = {
    registerAdminUser({ commit }, { form, bvToast, router }) {

        register.registerAdminUser(
            form,
            bvToast,
            () => router.push({ path: '/' }),
            () => {
                commit('setRegisterStatus', 'failed')
            }
        )
    },
    registerUser({ commit }, { form, bvToast, router }) {
        register.registerUser(
            form,
            bvToast,
            () => router.push({ path: '/' }),
            () => {
                commit('setRegisterStatus', 'failed')
            }
        )
    }
}

const mutations = {
    setRegisterStatus(state, status) {
        state.registerStatus = status
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}